cd /tmp
PATCHDIR=~/patches
git clone ssh://git@c4science.ch/diffusion/6356/domain-adaptation-residual-parameter-transfer.git residual_parameter_transfer
cd residual_parameter_transfer

# python train_joint_mnist.py --opt adam --lr 1e-5 --i 10000 --freq 100 --dc 1 --lmbd_dc 1.0 --deep_model resnet --dataset OFFICE  --li -1 2>&1 | tee ~/public_html/log.txt
# ERROR: RuntimeError: Could not open file /cvlabdata1/cvlab/forArtem/third_party/OFFICE/../Xlearn/caffe/models/JAN/resnet/deploy_grl_model.prototxt
# FIX:
# datasets.py.patch.1: Uncomment reference to Office path
#   120c120
#   <         #cfg.base_path        = osp.join('/','cvlabdata1','cvlab','forArtem','third_party','Office')
#   ---
#   >         cfg.base_path        = osp.join('/','cvlabdata1','cvlab','forArtem','third_party','Office')

patch lib/datasets.py ${PATCHDIR}/datasets.py.patch.1

# python train_joint_mnist.py --opt adam --lr 1e-5 --i 10000 --freq 100 --dc 1 --lmbd_dc 1.0 --deep_model resnet --dataset OFFICE  --li -1 2>&1 | tee ~/public_html/log.txt 1
# ERROR: google.protobuf.text_format.ParseError: 26:2 : Message type "caffe.LayerParameter" has no field named "batch_norm_param".
# FIX:
# train_joint_mnist.py.patch.1: use lib and lib_caffe2theano from the cloned repo instead of hardcoded /cvlabdata1 path
#   11,12c11,12
#   < sys.path.append(osp.join(code_path,'lib'))
#   < sys.path.append(osp.join(code_path,'lib_caffe2theano'))
#   ---
#   > sys.path.append(osp.join('.','lib'))
#   > sys.path.append(osp.join('.','lib_caffe2theano'))

cp /cvlabdata1/cvlab/forArtem/third_party/Xlearn/caffe/python/caffe/proto/caffe_pb2.py lib_caffe2theano
patch train_joint_mnist.py ${PATCHDIR}/train_joint_mnist.py.patch.1

# python train_joint_mnist.py --opt adam --lr 1e-5 --i 10000 --freq 100 --dc 1 --lmbd_dc 1.0 --deep_model resnet --dataset OFFICE  --li -1 2>&1 | tee ~/public_html/log.txt 1
# ERROR: IOError: [Errno 13] Permission denied: '/cvlabdata1/cvlab/forArtem/third_party/Office/models/unsupervised/dc_a2d_resnet/u_rn1/log.txt'
# FIX:
# train_joint_mnist.py.patch.2: store log at writeable location
#   274c274
#   <     log_file = save_pth+'log.txt'
#   ---
#   >     log_file = '/tmp/'+'log.txt'

patch train_joint_mnist.py ${PATCHDIR}/train_joint_mnist.py.patch.2

# python train_joint_mnist.py --opt adam --lr 1e-5 --i 10000 --freq 100 --dc 1 --lmbd_dc 1.0 --deep_model resnet --dataset OFFICE  --li -1 2>&1 | tee ~/public_html/log.txt 1
# ERROR: ValueError: GpuReshape: cannot reshape input of shape (513, 256) to shape (16, 256).
# FIX:
# datasets.py.patch.2: set batch size for Office dataset
#   141a142,143
#   >             cfg.src_bsz       = 8
#   >             cfg.tgt_bsz       = 8

patch lib/datasets.py ${PATCHDIR}/datasets.py.patch.2

# python train_joint_mnist.py --opt adam --lr 1e-5 --i 10000 --freq 100 --dc 1 --lmbd_dc 1.0 --deep_model resnet --dataset OFFICE  --li -1 2>&1 | tee ~/public_html/log.txt 1
# ERROR: IOError: [Errno 13] Permission denied: u'/cvlabdata1/cvlab/forArtem/third_party/Office/models/unsupervised/dc_a2d_resnet/u_rn1/layer_conv1_filters'
# FIX:
# datasets.py.patch.3: Save model to writeable location
#   245a246
#   >         cfg.save_path    = osp.join('.', 'models')

patch lib/datasets.py ${PATCHDIR}/datasets.py.patch.3

# python train_joint_mnist.py --opt adam --lr 1e-5 --i 10000 --freq 100 --dc 1 --lmbd_dc 1.0 --deep_model resnet --dataset OFFICE  --li -1 2>&1 | tee ~/public_html/log.txt 1
# RUNS!
